package rlr.consultaapi_github;

/**
 * Created by suerafa on 25/05/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PullRequestsList extends AppCompatActivity {

    private String TAG = PullRequestsList.class.getSimpleName();

    private ProgressDialog pDialog;
    ListView lv;

    //private static String url;
    static String url;

    ArrayList<HashMap<String, String>> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        itemList = new ArrayList<>();

        lv = (ListView) findViewById(R.id.list);

        url = getIntent().getStringExtra("repo");
        TextView txtTeste = (TextView) findViewById(R.id.txtTeste);
        txtTeste.setText(url.replace("https://api.github.com/", "").replace("repos/", ""));

        new PullRequestsList.GetItems().execute();

    }

    /**
     * Async task
     */
    private class GetItems extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(PullRequestsList.this);
            pDialog.setMessage("Carregando dados...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            Handler sh = new Handler();

            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {

                    //JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    //JSONArray items = jsonObj.getJSONArray("items");
                    JSONArray items = new JSONArray(jsonStr);


                    for (int i = 0; i < items.length(); i++) {
                        JSONObject c = items.getJSONObject(i);
                        //JSONArray c = items.getJSONArray(i);

                        String title = "Título:\n" + c.getString("title");
                        String body = "Body:\n" + c.getString("body");
                        String created_at = "Criado em: " + c.getString("created_at");
                        String updated_at = "Atualizado em: " + c.getString("updated_at");
                        String html_url = c.getString("html_url");

                        JSONObject user = c.getJSONObject("user");
                        String login = "User Login: " + user.getString("login");
                        String avatar_url = "https://avatars.githubusercontent.com/"+user.getString("login");

                        HashMap<String, String> item = new HashMap<>();

                        item.put("title", title);
                        item.put("body", body);
                        item.put("created_at", created_at);
                        item.put("updated_at", updated_at);
                        item.put("login", login);
                        item.put("html_url", html_url);
                        item.put("avatar_url", avatar_url);

                        itemList.add(item);
                    }

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Error from JSON server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Error to get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;

        }



    /* INCLUÍDO */

        public class MyAdapter extends SimpleAdapter{

            public MyAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to){
                super(context, data, resource, from, to);
            }

            public View getView(int position, View convertView, ViewGroup parent){
                // here you let SimpleAdapter built the view normally.
                View v = super.getView(position, convertView, parent);

                // Then we get reference for Picasso
                ImageView img = (ImageView) v.getTag();
                if(img == null){
                    img = (ImageView) v.findViewById(R.id.avatar_url);
                    v.setTag(img); // <<< THIS LINE !!!!
                }

                String url2 = (String) ((Map)getItem(position)).get("avatar_url");

                Picasso.with(v.getContext()).load(url2).resize(100, 100).into(img);

                // return the view
                return v;
            }
        }

    /* FIM INCLUÍDO */



        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

           /*
           SimpleAdapter adapter = new SimpleAdapter(
                    PullRequestsList.this,
                    itemList,
                    R.layout.activity_pull_requests_list_item,
                    new String[]{"title", "body", "created_at", "updated_at", "login", "html_url"},
                    new int[]{ R.id.title, R.id.body, R.id.created_at, R.id.updated_at, R.id.login, R.id.html_url});

            lv.setAdapter(adapter);
            */

           /* INCLUÍDO */

            ListAdapter adapter =
                    new MyAdapter(
                            PullRequestsList.this,
                            itemList,
                            R.layout.activity_pull_requests_list_item,
                            new String[]{"title", "body", "created_at", "updated_at", "login", "html_url"},
                            new int[]{ R.id.title, R.id.body, R.id.created_at, R.id.updated_at, R.id.login, R.id.html_url});
            lv.setAdapter(adapter);
            /* INCLUÍDO */

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String endereco = ((TextView) view.findViewById(R.id.html_url)).getText().toString();
                    Uri uri = Uri.parse(endereco.toString());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });

        }

    }

}