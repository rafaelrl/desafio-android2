package rlr.consultaapi_github;

/**
 * Created by suerafa on 31/05/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchResultsList extends AppCompatActivity {

    private String TAG = SearchResultsList.class.getSimpleName();

    private ProgressDialog pDialog;
    ListView lv;

    //private static String url;
    static String url;

    int numero = 1;
    JSONArray items;

    ArrayList<HashMap<String, String>> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        itemList = new ArrayList<>();

        lv = (ListView) findViewById(R.id.list);

        //url = getIntent().getStringExtra("repo");
        url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="+numero;
        //TextView txtTeste = (TextView) findViewById(R.id.txtTeste);
        //txtTeste.setText("Página: " + numero);

        new SearchResultsList.GetItems().execute();

    }

    /**
     * Async task
     */
    private class GetItems extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(SearchResultsList.this);
            pDialog.setMessage("Carregando dados...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            Handler sh = new Handler();

            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {

                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    items = jsonObj.getJSONArray("items");
                    //JSONArray items = new JSONArray(jsonStr);


                    for (int i = 0; i < items.length(); i++) {
                        JSONObject c = items.getJSONObject(i);
                        //JSONArray c = items.getJSONArray(i);

                        String posicao = "Página: " + numero + " - Item: " + (i+1);
                        String name = "Nome: " + c.getString("name");
                        String full_name = "Nome Repositório: " + c.getString("full_name");
                        String description = "Descrição Repositório:\n" + c.getString("description");
                        String forks_count = "Forks Count: " + c.getString("forks_count");
                        String stargazers_count = "Stargazers Count: " + c.getString("stargazers_count");

                        JSONObject owner = c.getJSONObject("owner");
                        String login = owner.getString("login");
                        String avatar_url = "https://avatars.githubusercontent.com/"+login;

                        HashMap<String, String> item = new HashMap<>();

                        item.put("posicao", posicao);
                        item.put("name", name);
                        item.put("full_name", full_name);
                        item.put("description", description);
                        item.put("forks_count", forks_count);
                        item.put("stargazers_count", stargazers_count);
                        item.put("avatar_url", avatar_url);

                        itemList.add(item);
                    }

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Error from JSON server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Error to get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;

        }


    /* INCLUÍDO */

        public class MyAdapter extends SimpleAdapter {

            public MyAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to){
                super(context, data, resource, from, to);
            }

            public View getView(int position, View convertView, ViewGroup parent){

                View v = super.getView(position, convertView, parent);

                ImageView img = (ImageView) v.getTag();
                if(img == null){
                    img = (ImageView) v.findViewById(R.id.avatar_url);
                    v.setTag(img);
                }

                String url2 = (String) ((Map)getItem(position)).get("avatar_url");

                Picasso.with(v.getContext()).load(url2).resize(100, 100).into(img);

                return v;
            }
        }

    /* FIM INCLUÍDO */



        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (pDialog.isShowing())
                pDialog.dismiss();

           /*
           SimpleAdapter adapter = new SimpleAdapter(
                    PullRequestsList.this,
                    itemList,
                    R.layout.activity_pull_requests_list_item,
                    new String[]{"title", "body", "created_at", "updated_at", "login", "html_url"},
                    new int[]{ R.id.title, R.id.body, R.id.created_at, R.id.updated_at, R.id.login, R.id.html_url});

            lv.setAdapter(adapter);
            */

           /* INCLUÍDO */
            ListAdapter adapter =
                    new MyAdapter(
                            SearchResultsList.this,
                            itemList,
                            R.layout.activity_search_results_list_item,
                            new String[]{"posicao", "name", "full_name", "description", "forks_count", "stargazers_count"},
                            new int[]{ R.id.posicao, R.id.name, R.id.full_name, R.id.description, R.id.forks_count, R.id.stargazers_count});
            lv.setAdapter(adapter);
            /* FIM INCLUÍDO */

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent i = new Intent(SearchResultsList.this, PullRequestsList.class);
                    try {
                        i.putExtra("repo", "https://api.github.com/repos/"+items.getJSONObject(position).getString("full_name")+"/pulls");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(i);

                }
            });

        }

    }

}